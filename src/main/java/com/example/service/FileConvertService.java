package com.example.service;

import com.example.dao.UploadDataDTO;

import java.util.List;
import java.util.Map;

public interface FileConvertService {

	Map<String, List<UploadDataDTO>> convertUploadFile(String fileName);


}
