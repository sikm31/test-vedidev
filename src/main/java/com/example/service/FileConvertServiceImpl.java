package com.example.service;

import com.example.dao.UploadDataDTO;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FileConvertServiceImpl implements FileConvertService {

	private static final Logger LOG = LoggerFactory.getLogger(FileConvertServiceImpl.class);

	@Override
	public Map<String, List<UploadDataDTO>> convertUploadFile(String fileName) {
		Map<String, List<UploadDataDTO>> uploadFileConvertMap = new HashMap<>();
		try (FileInputStream fileInputStream = new FileInputStream(new File(fileName))) {
			MappingIterator<UploadDataDTO> csvUpload = readUploadFromCsv(fileInputStream);
			getConvertFileMap(uploadFileConvertMap, csvUpload);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return uploadFileConvertMap;
	}

	private void getConvertFileMap(Map<String, List<UploadDataDTO>> uploadFileConvertMap, MappingIterator<UploadDataDTO> csvUpload) throws IOException {
		for (UploadDataDTO uploadDataDTO : csvUpload.readAll()) {
			if (uploadDataDTO.getSsoid().isEmpty()) continue;
			List<UploadDataDTO> tempDTOList = new ArrayList<>();
			if (uploadFileConvertMap.keySet().contains(uploadDataDTO.getSsoid())) {
				tempDTOList = uploadFileConvertMap.get(uploadDataDTO.getSsoid());
				tempDTOList.add(uploadDataDTO);
			} else {
				tempDTOList.add(uploadDataDTO);
			}
			uploadFileConvertMap.put(uploadDataDTO.getSsoid().toLowerCase().trim(), tempDTOList);
		}
	}

	private MappingIterator<UploadDataDTO> readUploadFromCsv(InputStream csvStream) throws IOException {
		CsvSchema schema = CsvSchema.emptySchema()
				.withHeader()
				.withColumnSeparator(';')
				.withNullValue("null")
				.withoutEscapeChar();

		return new CsvMapper().readerFor(UploadDataDTO.class).with(schema).readValues(csvStream);
	}

}
