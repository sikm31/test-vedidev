package com.example.service;

import com.example.dao.FormDataDAO;
import com.example.model.FormDataModel;
import com.example.util.ConvertDtoFormData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class FormDataServiceImpl implements FormDataService {

	private final FileConvertService fileConvertService;

	private final FormDataDAO dao;

	@Autowired
	public FormDataServiceImpl(FileConvertService fileConvertService, FormDataDAO dao) {
		this.fileConvertService = fileConvertService;
		this.dao = dao;
	}


	@Override
	public void addFormData(String fileName) {
		ConvertDtoFormData.getConvertFormData(fileConvertService.convertUploadFile(fileName))
				.forEach(dao::addFormData);
	}

	@Override
	public List<FormDataModel> getFromDataTSLastHour() {
		Instant instant = Instant.now();
		Instant instantHourEarlier = instant.minus(1, ChronoUnit.HOURS);
		return dao.getFromDataTS(instantHourEarlier.getEpochSecond());
	}

	@Override
	public List<String> getFromDataTopFive() {
		return dao.getFromDataTopFive();
	}


}
