package com.example.service;

import com.example.model.FormDataModel;

import java.util.List;

public interface FormDataService {

	void addFormData(String fileName);

	List<FormDataModel> getFromDataTSLastHour();

	List<String> getFromDataTopFive();
}
