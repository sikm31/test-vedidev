package com.example.model;


import lombok.*;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name = "users")
@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserModel {

	@Id
	@Column
	private String ssoid;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private Set<FormDataModel> formDataModelList;

}
