package com.example.model;

import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "uploadData")
@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FormDataModel {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "sso_id", nullable = false)
	private UserModel user;

	private Long ts;
	private String grp;
	private String type;
	private String subtype;
	private String url;
	private String orgid;
	private String formid;
	private String code;
	private String ltpa;
	private String sudirresponse;
	private String ymdh;




}
