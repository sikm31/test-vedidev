package com.example.dao;

import com.example.model.FormDataModel;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


@Repository
@Transactional
public class FormDataDAOImpl implements FormDataDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void addFormData(FormDataModel dataModel) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(dataModel.getUser());
		session.saveOrUpdate(dataModel);

	}

	@Override
	public List<FormDataModel> getFromDataTS(Long ts) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<FormDataModel> query = builder.createQuery(FormDataModel.class);
		Root<FormDataModel> modelRoot = query.from(FormDataModel.class);
		query.select(modelRoot).where(builder.greaterThanOrEqualTo(modelRoot.get("ts"), ts));
		List<FormDataModel> modelList = entityManager.createQuery(query).getResultList();
		return modelList;
	}

	@Override
	public List<String> getFromDataTopFive() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<String> query = builder.createQuery(String.class);
		Root<FormDataModel> root = query.from(FormDataModel.class);
		query.select(root.get("formid")).where(builder.notEqual(root.get("formid"), ""));
		query.groupBy(root.get("formid"));
		query.orderBy(builder.desc(builder.count(root.get("formid"))));
		List<String> modelList = entityManager.createQuery(query).getResultList().subList(0, 5);
		return modelList;
	}


}
