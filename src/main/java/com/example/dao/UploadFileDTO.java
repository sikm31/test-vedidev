package com.example.dao;

import org.springframework.web.multipart.MultipartFile;

public class UploadFileDTO {

	private MultipartFile file;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
}
