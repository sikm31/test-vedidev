package com.example.dao;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadDataDTO {
	private String ssoid;
	private Long ts;
	private String grp;
	private String type;
	private String subtype;
	private String url;
	private String orgid;
	private String formid;
	private String code;
	private String ltpa;
	private String sudirresponse;
	private String ymdh;
}
