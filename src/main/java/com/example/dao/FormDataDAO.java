package com.example.dao;

import com.example.model.FormDataModel;

import java.util.List;

public interface FormDataDAO {

	void addFormData(FormDataModel dataModel);
	List<FormDataModel> getFromDataTS(Long ts);
	List<String> getFromDataTopFive();

}
