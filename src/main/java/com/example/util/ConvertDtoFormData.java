package com.example.util;

import com.example.dao.UploadDataDTO;
import com.example.model.FormDataModel;
import com.example.model.UserModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ConvertDtoFormData {

	public static List<FormDataModel> getConvertFormData(Map<String, List<UploadDataDTO>> listMap) {
		List<FormDataModel> dataModelList = new ArrayList<>();

		for (Map.Entry<String, List<UploadDataDTO>> entry : listMap.entrySet()) {
			UserModel user = UserModel.builder().
					ssoid(entry.getKey()).build();
			user.setSsoid(entry.getKey());
			for (UploadDataDTO dataDTO : entry.getValue()) {
				FormDataModel model = FormDataModel.builder().
						user(user).
						ts(dataDTO.getTs()).
						grp(dataDTO.getGrp()).
						type(dataDTO.getType()).
						subtype(dataDTO.getSubtype()).
						url(dataDTO.getUrl()).
						orgid(dataDTO.getOrgid()).
						formid(dataDTO.getFormid()).
						code(dataDTO.getCode()).
						ltpa(dataDTO.getLtpa()).
						sudirresponse(dataDTO.getSudirresponse()).
						ymdh(dataDTO.getYmdh()).build();
				dataModelList.add(model);
			}

		}
		return dataModelList;
	}

}
