<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>The five most used forms</title>
    <link href="<c:url value='https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css' />"  rel="stylesheet"></link>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body>
<div class="container">
    <table class="table table-striped table-bordered">
        <tr>
            <th>Top Five Form id</th>
        </tr>
    <c:forEach var="topFive" items="${mostUsedForms}">
        <tr>
            <td>${topFive}</td>
        </tr>
    </c:forEach>
    <br/><br/>
    <a href="<c:url value='/home' />">Home</a>
    </table>
</div>
</body>
</html>
