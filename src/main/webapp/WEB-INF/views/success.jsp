<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>File Upload Success</title>
	<link href="<c:url value='https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body>
	<div class="success">
		File  <strong>${fileName}</strong> uploaded successfully.
		<br/><br/>
		<a href="<c:url value='/home' />">Home</a>
	</div>
</body>
</html>