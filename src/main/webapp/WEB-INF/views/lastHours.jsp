<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users and forms in the last hour</title>
    <link href="<c:url value='https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css' />"  rel="stylesheet"></link>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body>
<div class="container">
    <table class="table table-striped table-bordered">
        <tr>
            <th>Id unique user</th>
            <th>Time</th>
            <th>Group event</th>
            <th>Type event</th>
            <th>SubType event</th>
            <th>Url</th>
            <th>Organization id</th>
            <th>Code</th>
            <th>Form id</th>
            <th>Session key</th>
            <th>Response service</th>
            <th>Date</th>
        </tr>
    <c:forEach var="lastHour" items="${listDataHour}">
        <tr>
            <td>${lastHour.user.ssoid}</td>
            <td>${lastHour.ts}</td>
            <td>${lastHour.grp}</td>
            <td>${lastHour.type}</td>
            <td>${lastHour.subtype}</td>
            <td>${lastHour.url}</td>
            <td>${lastHour.orgid}</td>
            <td>${lastHour.formid}</td>
            <td>${lastHour.code}</td>
            <td>${lastHour.ltpa}</td>
            <td>${lastHour.sudirresponse}</td>
            <td>${lastHour.ymdh}</td>
        </tr>
    </c:forEach>
    <br/><br/>
    <a href="<c:url value='/home' />">Home</a>
</div>
</body>
</html>
