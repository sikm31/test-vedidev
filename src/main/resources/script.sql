create table if not exists users
(
  ssoid varchar(255) not null
    constraint users_pkey
    primary key
);

alter table users
  owner to postgres;

create table if not exists uploaddata
(
  id            bigserial    not null
    constraint uploaddata_pkey
    primary key,
  code          varchar(255),
  formid        varchar(255),
  grp           varchar(255),
  ltpa          varchar(255),
  orgid         varchar(255),
  subtype       varchar(255),
  sudirresponse varchar(255),
  ts            bigint,
  type          varchar(255),
  url           varchar(255),
  ymdh          varchar(255),
  sso_id        varchar(255) not null
    constraint fkh7bchmg748kv9agimtqnsnwui
    references users
);

alter table uploaddata
  owner to postgres;

